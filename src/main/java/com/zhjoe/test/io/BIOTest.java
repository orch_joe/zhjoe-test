package com.zhjoe.test.io;

import cn.hutool.log.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @Author: qiaoshipeng
 * @Date: 2021-06-08 11:07
 */
public class BIOTest {
    public static Log log = Log.get();
    // 默认监听端口号
    public static int port = 7777;

    public static void main(String[] args) throws IOException {
        /**
         * 生成bio客户端 -->
         * linux 操作系统执行:
         * socket() = 5fd;
         * bind(5fd,7070)
         * listen(5fd)
         */
        ServerSocket ss = new ServerSocket(port);
        log.info("ServerSocket服务器启动成功，等待接收客户端......");

        // 如果服务器没有关闭，死循环
        while (!ss.isClosed()) {
            /**
             * 等待接收客户端
             * 相当于linux系统执行了操作
             * accept(5fd)=7fd生成新的文件标识符
             */
            Socket socket = ss.accept(); //阻塞
            log.info("ServerSocket接收到链接[{}:{}]", socket.getInetAddress(), socket.getPort());
            //接收到消息
            InputStream inputStream = socket.getInputStream();
            //使用buffer reader 读取input信息
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            String msg;
            //如果没有接收到消息，那么就是阻塞状态
            while ((msg = reader.readLine()) != null) { // 没有数据，阻塞
                if (msg.length() == 0) {
                    break;
                }
                log.info("ServerSocket接收链接[{}:{}]发来的消息：{}", socket.getInetAddress(), socket.getPort(), msg);
            }
            log.info("ServerSocket接收断开连接[{}:{}]", socket.getInetAddress(), socket.getPort());

        }
    }
}
