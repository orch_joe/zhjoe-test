package com.zhjoe.test.io;

import cn.hutool.log.Log;
import com.google.common.collect.Lists;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.List;

/**
 * @Author: qiaoshipeng
 * @Date: 2021-06-08 11:07
 */
public class NIOTest {
    public static Log log = Log.get();
    // 默认监听端口号
    public static int port = 7777;
    public static List<SocketChannel> socketChannels = Lists.newArrayList();

    public static void main(String[] args) throws IOException, InterruptedException {
        //打开一个服务端
        ServerSocketChannel ssc = ServerSocketChannel.open();
        //服务端绑定端口
        ssc.bind(new InetSocketAddress(port));
        //服务端是否阻塞Blocking 是否阻塞：true，false
        ssc.configureBlocking(false);
        int i = 0;
        while (ssc.isOpen()) {
            //睡眠一秒
            Thread.sleep(1000);
            //非阻塞
            SocketChannel sc = ssc.accept();
            if (null == sc) {
                log.debug("等待客户端连接{}......", (i++));
            } else {
                //socket通道是否阻塞
                sc.configureBlocking(false);
                int port = sc.socket().getPort();
                InetAddress inetAddress = sc.socket().getInetAddress();
                log.info("ServerSocket接收到链接[{}:{}]", inetAddress, port);
                socketChannels.add(sc);
            }
            /**
             * allocate方式创建的ByteBuffer对象我们称之为非直接缓冲区，这个ByteBuffer对象(和对象包含的缓冲数组)都位于JVM的堆区。
             * wrap方式和allocate方式创建的ByteBuffer没有本质区别，都创建的是非直接缓冲区。
             *
             * allocateDirect方法创建的ByteBuffer我们称之为直接缓冲区，此时ByteBuffer对象本身在堆区，而缓冲数组位于非堆区
             * ByteBuffer对象内部存储了这个非堆缓冲数组的地址。在非堆区的缓冲数组可以通过JNI（内部还是系统调用）方式进行IO操作，JNI不受gc影响，机器码执行速度也比较快，
             * 同时还避免了JVM堆区与操作系统内核缓冲区的数据拷贝，所以IO速度比非直接缓冲区快。
             * 然而allocateDirect方式创建ByteBuffer对象花费的时间和回收该对象花费的时间比较多，所以这个方法适用于创建那些需要重复使用的缓冲区对象。
             */
            //直接缓存区-->不占用jvm的堆区
            ByteBuffer receiveBuffer = ByteBuffer.allocateDirect(4096);
            try {
                for (SocketChannel socketChannel : socketChannels) {
                    int num = socketChannel.read(receiveBuffer);
                    //说明有数据包
                    if (num > 0) {
                        //内存指针复位移动到开始位置，并且标记结束位置的limit
                        receiveBuffer.flip();
                        //定义一个byte数组，大小为指针移动到limit位置
                        byte[] bytes = new byte[receiveBuffer.limit()];
                        //获取到从开始标记位置，到结束标记位置的内容
                        receiveBuffer.get(bytes);
                        String content = new String(bytes);
                        log.info("socketChannel接收链接[{}:{}]发来的消息：{}", socketChannel.socket().getInetAddress(), socketChannel.socket().getPort(), content);
                    } else if (num == 0) {
                        log.info("socketChannel 接收链接[{}:{}],等待发送消息", socketChannel.socket().getInetAddress(), socketChannel.socket().getPort());
                    } else {
                        log.info("socketChannel断开连接[{}:{}]", socketChannel.socket().getInetAddress(), socketChannel.socket().getPort());
                        socketChannels.remove(socketChannel);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }

    }
}
