package com.zhjoe;


import cn.hutool.log.Log;

/**
 * 系统入口
 */
public class App {
    public static Log log = Log.get();

    /**
     * 系统入口
     * @param args
     */
    public static void main(String[] args) {
        log.info("app run start");

        try {

        } catch (Exception e) {
            e.printStackTrace();
            log.info("app exception");
        }

        log.info("app run end");

    }
}
